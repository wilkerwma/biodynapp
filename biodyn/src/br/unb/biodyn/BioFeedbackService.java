package br.unb.biodyn;

import java.util.TreeMap;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import br.unb.biodyn.model.BioFeedback;
import br.unb.biodyn.model.VibratoryBioFeedBack;

public class BioFeedbackService extends Service {

	public static final String ACTION_BIO_FEEDBACK_SERVICE = "br.unb.biodyn.BioFeedbackService";

	public static final int BIO_FEEDBACK_VIBRATORY = 0x0001;

	private TreeMap<Integer, BioFeedback> bioFeedbackMap;
	private MyReceiver myReceiver;

	@Override
	public void onCreate() {
		myReceiver = new MyReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_START);
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_STOP);
		LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
				filter);

		this.bioFeedbackMap = new TreeMap<Integer, BioFeedback>();
		this.bioFeedbackMap.put(
				Communicator.BioFeedbackServiceActions.FLAG_VIBRATION,
				new VibratoryBioFeedBack(this));

		super.onCreate();
	}

	@Override
	public void onDestroy() {

		LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
		super.onDestroy();
	}

	private void startBioFeedbacks(int flags, long time) {
		synchronized (this.bioFeedbackMap) {
			for (Integer integer : this.bioFeedbackMap.keySet()) {
				if ((flags & integer) != 0) {
					this.bioFeedbackMap.get(integer).start(time);
				}
			}
		}
	}

	private void stopBioFeedbacks(int flags) {
		synchronized (this.bioFeedbackMap) {
			for (Integer integer : this.bioFeedbackMap.keySet()) {
				if ((flags & integer) != 0) {
					this.bioFeedbackMap.get(integer).stop();
				}
			}
		}
	}

	private class MyReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action
					.equals(Communicator.BioFeedbackServiceActions.ACTION_START)) {
				long time = intent
						.getExtras()
						.getLong(
								Communicator.BioFeedbackServiceActions.KEY_BIOFEEDBACK_TIME);
				int flags = intent.getExtras().getInt(
						Communicator.BioFeedbackServiceActions.KEY_FLAGS);
				startBioFeedbacks(flags, time);
			} else if (action
					.equals(Communicator.BioFeedbackServiceActions.ACTION_STOP)) {
				int flags = intent.getExtras().getInt(
						Communicator.BioFeedbackServiceActions.KEY_FLAGS);
				stopBioFeedbacks(flags);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
