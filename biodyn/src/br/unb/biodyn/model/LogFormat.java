
package br.unb.biodyn.model;

import android.util.Log;

public class LogFormat {

	public static final String EXERCISE = "exercise";
	public static final String BLUETOOTHSERVICE = "bluetoothService";
	public static final String COMMUNICATOR = "communicator";
	public static final String DEVICE_LIST_ACTIVITY= "deviceListActivity";
	public static final String MAIN_ACTIVITY = "mainActivity";

	private static final boolean DEBUG = false;
	
	public static void setLogMessage (String tag, String message) {
		if(DEBUG) {
			Log.d(tag, message);
		}	
	}
	
	public static void setLogMessageException (String tag, String message, Exception e) {
		if(DEBUG) {
			Log.e(tag, message,e);
		}
	}
}
