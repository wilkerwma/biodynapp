package br.unb.biodyn.model;

import android.os.CountDownTimer;
import android.util.Log;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.ui.StartTimer;

public class IsometricExercise extends Exercise {

	/********************************** CONSTANTS ***********************************/
	private static final String TAG = "IsometricExercise";
	private static final float PERCENT_ERROR_MAXIMUM = 0.9f;
	private static final float PERCENT_ERROR_MINIMUM = 1.1f;
	private static final int ONE_SECOND = 1000;

	private static final float DEFAULT_MAXIMUM_WEIGHT = 6;
	private static final float DEFAULT_MINIMUM_WEIGHT = 4;
	private static int DEFAULT_REPETITIONS = 10;
	private static int DEFAULT_SERIES = 5;
	private static final int DEFAULT_REST_TIME = 30000;
	private static final long DEFAULT_HOLD_TIME = 5000;

	/********************************** CLASS FIELDS ***********************************/

	private float realWeight;
	private boolean checkMaximumWeight;
	private boolean checkMinimumWeight;
	private boolean restTime = false;
	private int initialRepetitions;

	Timer time = new Timer();
	StartTimer startTimer = new StartTimer(DEFAULT_HOLD_TIME, DEFAULT_HOLD_TIME);

	/********************************** CLASS METHODS ***********************************/

	public IsometricExercise(ExerciseData data) {
		super(data);
	}

	public IsometricExercise() {
		this(new ExerciseData(DEFAULT_MAXIMUM_WEIGHT, DEFAULT_MINIMUM_WEIGHT,
				DEFAULT_REPETITIONS, DEFAULT_SERIES, DEFAULT_REST_TIME,
				DEFAULT_HOLD_TIME));
	}

	@Override
	public boolean checkRepetition(float data) {
		realWeight = data;
		
			return holdTime == 0;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
	}

	@Override
	public void countDownRepetitions() {
		repetitions--;

		if (restTime) {
			restTime = false;
			Communicator.getInstance().sendToUI(Communicator.FLAG_STOP_TIMER,
					this);
		}

		Communicator.getInstance()
				.sendToUI(Communicator.FLAG_REPETITIONS, this);

	}

	@Override
	public void countDownSeries() {
		this.series--;

		if (series == 0) {
			reset();
			stop();
		} else {
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_TIMER,
					this);

			restTime = true;
		}
		repetitions = initialRepetitions;

		Communicator.getInstance().sendToUI(
				Communicator.FLAG_SERIES | Communicator.FLAG_REPETITIONS, this);
	}

	public boolean checkRangeWeightOfExercise() {
		checkMinimumRange();
		checkMaximumRange();

		return checkMaximumWeight && checkMinimumWeight;
	}

	private void checkMinimumRange() {
		if (realWeight >= minimumWeight * PERCENT_ERROR_MINIMUM) {
			this.checkMinimumWeight = true;
			Log.d("debug", "checkFirstMinimumValue");
		}
	}

	private boolean checkMaximumRange() {
		if (realWeight >= maximumWeight * PERCENT_ERROR_MAXIMUM) {
			this.checkMaximumWeight = true;
			Log.d("debug", "checkMaximumRange");
		}
		return true;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}


}
