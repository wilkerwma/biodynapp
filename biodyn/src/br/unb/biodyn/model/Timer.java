package br.unb.biodyn.model;

import br.unb.biodyn.Communicator;
import br.unb.biodyn.ui.StartTimer;
import br.unb.biodyn.ui.StartTimer.StartTimerListener;

public class Timer implements StartTimerListener {

	private static final long ONE_SECOND = 1000;
	private StartTimer startTimer;
	
	private long duration;
	private long actualTime;

	private long onInterval = 1000;
	
	public Timer() {
		this.startTimer = new StartTimer(actualTime, onInterval);
		this.startTimer.setStartTimerListener(this);
	}
	
	
	@Override
	public void onTick(StartTimer startTimer, long millisUntilFinished) {
		returnCurrentTime();
		actualTime = (actualTime - ONE_SECOND);
	}

	@Override
	public void onFinish(StartTimer startTimer) {
		Communicator.getInstance().sendToBioFeedback(
				Communicator.BioFeedbackServiceActions.ACTION_START,
				Communicator.BioFeedbackServiceActions.FLAG_VIBRATION, 1000);
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
		this.actualTime = duration;
		this.startTimer.cancel();
	}
	
	public long getDuration() {
		return duration;
	}

	public void startTimer(long time) {
		this.actualTime = this.duration = time;
		this.startTimer.cancel();
		this.startTimer = new StartTimer(this.duration, this.onInterval);
		this.startTimer.setStartTimerListener(this);
		this.startTimer.start();
	}

	public void stopTimer() {
		this.startTimer.cancel();
		actualTime = 0;
		returnCurrentTime();
	}

	public long returnCurrentTime(){
		LogFormat.setLogMessage(LogFormat.EXERCISE,"1");
		return actualTime;
	}
	
}
