package br.unb.biodyn.model;

public interface ExerciseInterface {

	public abstract boolean checkRepetition(float data);

	public abstract void reset();

	public abstract void countDownRepetitions();

	public abstract void countDownSeries();

	public abstract void stop();
}
