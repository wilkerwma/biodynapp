package br.unb.biodyn.model;

public class ExerciseData {
	
	protected float maximumWeight;
	protected float minimumWeight;
	protected int repetitions;
	protected int series;
	protected long restTime;
	protected long holdTime;
	
	public ExerciseData(float maximumWeight, float minimumWeight, int repetitions, int series, long restTime, long holdTime) {
		this.maximumWeight = maximumWeight;
		this.minimumWeight = minimumWeight;
		this.repetitions = repetitions;
		this.series = series;
		this.restTime = restTime;
		this.holdTime = holdTime;
	}
	
	public ExerciseData() {
		this(0, 0, 0, 0, 0, 0);
	}
	
	public long getHoldTime() {
		return holdTime;
	}

	public void setHoldTime(long holdTime) {
		this.holdTime = holdTime;
	}

	public float getMaximumWeight() {
		return maximumWeight;
	}
	public void setMaximumWeight(float maximumWeight) {
		this.maximumWeight = maximumWeight;
	}
	public float getMinimumWeight() {
		return minimumWeight;
	}
	public void setMinimumWeight(float minimumWeight) {
		this.minimumWeight = minimumWeight;
	}
	public int getRepetitions() {
		return repetitions;
	}
	public void setRepetitions(int repetitions) {
		this.repetitions = repetitions;
	}
	public int getSeries() {
		return series;
	}
	public void setSeries(int series) {
		this.series = series;
	}
	public long getRestTime() {
		return restTime;
	}
	public void setRestTime(long restTime) {
		this.restTime = restTime;
	}

	@Override
	public String toString() {
		return "ExerciseData [maximumWeight=" + maximumWeight + ", minimumWeight=" + minimumWeight + ", repetitions="
				+ repetitions + ", series=" + series + ", restTime=" + restTime + "]";
	}
}
