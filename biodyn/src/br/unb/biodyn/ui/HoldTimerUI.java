package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.model.ExerciseData;
import br.unb.biodyn.ui.StartTimer.StartTimerListener;

public class HoldTimerUI extends TimerUI {

	public HoldTimerUI(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onTick(StartTimer startTimer, long millisUntilFinished) {
		super.onTick(startTimer, millisUntilFinished);

		ExerciseData exerciseData = new ExerciseData();

		exerciseData.setHoldTime(getActualTime());

		Communicator.getInstance().sendToExerciseService(
				Communicator.FLAG_HOLD_TIME, exerciseData);
	}

}
