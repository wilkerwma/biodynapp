package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.unb.biodyn.R;
import br.unb.biodyn.model.LogFormat;

public class CounterUI extends RelativeLayout implements OnClickListener {

	public interface CounterUIListener {
		public void onValueChange(CounterUI counterUI);
	}

	private TextView textCounter;
	private Button buttonIncrease;
	private Button buttonDecrease;
	private Integer counter;

	private CounterUIListener listener;

	public CounterUI(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.counter_components, this);
		this.counter = 0;
		initializeComponents();
		updateTextCounter();
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == this.buttonIncrease.getId()) {
			increase();
		} else if (view.getId() == this.buttonDecrease.getId()) {
			decrease();
		}
	}

	public void increase() {
		this.counter++;
		updateTextCounter();
		notifyListenerOnValueChange();
		LogFormat.setLogMessage(LogFormat.EXERCISE, "increase");
	}

	public void decrease() {
		this.counter--;
		updateTextCounter();
		notifyListenerOnValueChange();
	}

	public void setValue(int counter) {
		this.counter = counter;
		updateTextCounter();
		notifyListenerOnValueChange();
		LogFormat.setLogMessage(LogFormat.EXERCISE, "setValue");
	}

	private void updateTextCounter() {
		StringBuilder sb = new StringBuilder();
		if (counter > 0)
			sb.append(counter);
		else {
			sb.append(0);
			sb.append(" + ");
			sb.append(-counter);
		}
		this.textCounter.setText(sb.toString());
	}

	private void initializeComponents() {
		this.textCounter = (TextView) findViewById(R.id.text_counter);
		this.buttonDecrease = (Button) findViewById(R.id.button_decrease);
		this.buttonDecrease.setOnClickListener(this);
		this.buttonIncrease = (Button) findViewById(R.id.button_increase);
		this.buttonIncrease.setOnClickListener(this);
	}

	public void setOnClickListener(OnClickListener listener) {
		this.buttonIncrease.setOnClickListener(listener);
		this.buttonDecrease.setOnClickListener(listener);
	}

	public Integer getValue() {
		return this.counter;
	}

	public void setCounterUIListener(CounterUIListener listener) {
		this.listener = listener;
	}

	private void notifyListenerOnValueChange() {
		if(this.listener != null) {
			this.listener.onValueChange(this);
		}
	}

}
