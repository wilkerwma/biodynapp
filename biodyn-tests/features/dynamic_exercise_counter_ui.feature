Feature: Touch Counters Buttons

	Scenario: Press increase button of Series
		When I press view with id "button_increase" in counter_ui with id "series_counter"
		Then I should see "6"
	
	Scenario: Press decrease button of Series
		When I press view with id "button_decrease" in counter_ui with id "series_counter"
		Then I should see "4"

	Scenario: Press increase button of Repetitions
		When I press view with id "button_increase" in counter_ui with id "repetition_counter"
		Then I should see "11"
	
	Scenario: Press decrease button of Repetitions
		When I press view with id "button_decrease" in counter_ui with id "repetition_counter"
		Then I should see "9"
	
	Scenario: Press increase button of Rest Timer
		When I press view with id "button_increase" in counter_ui with id "timer"
		Then I should see "011"
	
	Scenario: Press decrease button of Rest Timer
		When I press view with id "button_decrease" in counter_ui with id "timer"
		Then I should see "009"
	
	Scenario: Press increase button of Superior Limit
		When I press view with id "button_increase" in counter_ui with id "counter_superior_limit"
		Then I should see "11"
	
	Scenario: Press decrease button of Superior Limit
		When I press view with id "button_decrease" in counter_ui with id "counter_superior_limit"
		Then I should see "9"
	
	Scenario: Press increase button of Inferior Limit
		When I press view with id "button_increase" in counter_ui with id "counter_inferior_limit"
		Then I should see "3"
	
	Scenario: Press decrease button of Inferior Limit
		When I press view with id "button_decrease" in counter_ui with id "counter_inferior_limit"
		Then I should see "1"
	
