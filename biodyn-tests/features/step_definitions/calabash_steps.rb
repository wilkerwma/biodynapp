require 'calabash-android/calabash_steps'

Then /^I press view with id "([^\"]*)" in counter_ui with id "([^\"]*)"$/ do |view_id, counter_id|
 	tap_when_element_exists("* id:'#{counter_id}' Button id:'#{view_id}'")
end