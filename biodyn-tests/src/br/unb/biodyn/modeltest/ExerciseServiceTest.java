package br.unb.biodyn.modeltest;

import br.unb.biodyn.Communicator;
import br.unb.biodyn.ExerciseService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.test.ServiceTestCase;
import android.test.mock.MockContext;

public class ExerciseServiceTest extends ServiceTestCase<ExerciseService> {

	public ExerciseServiceTest() {
		super(ExerciseService.class);
	}
	
	Intent intent;

	@Override
	protected void setUp() throws Exception {
		Communicator.initialize(LocalBroadcastManager
				.getInstance(getContext()));
		intent = new Intent(getContext(), ExerciseService.class);
		super.setUp();
	}

	public void testCreateService() throws Exception {
		intent.setClass(getContext(), ExerciseService.class);
		startService(intent);
	}

	
}
