package br.unb.biodyn.modeltest;

import android.support.v4.content.LocalBroadcastManager;
import android.test.ActivityInstrumentationTestCase2;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.DeviceListActivity;
import br.unb.biodyn.model.DynamicExercise;
import br.unb.biodyn.model.ExerciseData;

public class DynamicExerciseTest extends ActivityInstrumentationTestCase2<DeviceListActivity> {

	private DynamicExercise exercise;
	private Float maximumWeight;
	private Float minimumWeight;

	public DynamicExerciseTest() {
		super(DeviceListActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		Communicator.initialize(LocalBroadcastManager.getInstance(getActivity()));
		this.maximumWeight = 10.0f;
		this.minimumWeight = 2.0f;

		ExerciseData exerciseData = new ExerciseData(maximumWeight, minimumWeight, 10, 10, 30);
		this.exercise = new DynamicExercise(exerciseData);

		this.exercise.setMaximumWeight(maximumWeight);
		this.exercise.setMinimumWeight(minimumWeight);
	}

	public void testcheckRepetitionWithWeightValueInLimit() throws Exception {
		Float weights[] = new Float[]{2.0f,5.0f,10.0f,5.0f,2.0f};

		for(int i = 0; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertTrue(this.exercise.checkRepetition(weights[weights.length-1]));
	}

	public void testcheckRepetitionWithWeightValueInLimitWithRange() throws Exception {
		Float weights[] = new Float[]{2.2f,5.0f,9.0f,5.0f,2.2f};

		for(int i = 0; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertTrue(this.exercise.checkRepetition(weights[weights.length-1]));
	}

	public void testCountTwoRepetition() throws Exception {
		Float weights[] = new Float[]{2.2f,5.0f,9.0f,5.0f,2.2f,2.0f,5.0f,10.0f,5.0f,2.0f};

		for(int i = 0; i < (weights.length/2) - 1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertTrue(this.exercise.checkRepetition(weights[weights.length/2]));

		for(int i = weights.length/2; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertTrue(this.exercise.checkRepetition(weights[weights.length-1]));
	}

	public void testcheckRepetitionWithFailureOnTheSecondMinimum() throws Exception {
		Float weights[] = new Float[]{2.2f,5.0f,9.0f,5.0f,2.3f};

		for(int i = 0; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertFalse(this.exercise.checkRepetition(weights[weights.length-1]));
	}

	public void testcheckRepetitionWithFailureOnTheMaximum() throws Exception {
		Float weights[] = new Float[]{2.2f,5.0f,7.0f,5.0f,2.2f};

		for(int i = 0; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertFalse(this.exercise.checkRepetition(weights[weights.length-1]));
	}

	public void testcheckRepetitionWithoutFirstMinimum() throws Exception {
		Float weights[] = new Float[]{1.2f,1.0f,1.0f,1.0f,1.3f};

		for(int i = 0; i < weights.length-1; i++) {
			assertFalse(this.exercise.checkRepetition(weights[i]));
		}
		assertFalse(this.exercise.checkRepetition(weights[weights.length-1]));
	}
}
